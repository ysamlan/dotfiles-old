alias reload!='. ~/.zshrc'
alias b='bundle exec'
alias bake='bundle exec rake'
alias lanip='ifconfig en0 | grep "inet " | cut -f 2 -d " "'
alias ppjson='underscore print --color'
alias vim='mvim -v'
alias bundlej="bundle -j4"
alias gsmu="git submodule update --init --recursive"
alias h='http -vj' # httpie: pretty-printing sane-parameter cURL alternative. pip install --upgrade httpie
alias dl='cd ~/Downloads'


export RUBY_GC_MALLOC_LIMIT=90000000
export RUBY_GC_HEAP_FREE_SLOTS=200000
export RUBY_GC_HEAP_INIT_SLOTS=2000000
export RUBY_HEAP_FREE_MIN=20000

# Mac Helpers
alias show_hidden="defaults write com.apple.Finder AppleShowAllFiles YES && killall Finder"
alias hide_hidden="defaults write com.apple.Finder AppleShowAllFiles NO && killall Finder"

# Android stuff
alias adbscreencap="adb shell screencap -p | perl -pe 's/\x0D\x0A/\x0A/g' > screen.png"
export ANDROID_HOME=~/opt/android-sdks/

# LevelUp-specific stuff
# autocomplete navigating to levelup projects using lu keyword (eg `luwa guides`) with autocomplete
lu() { cd ~/projects/levelup/$1; }
compctl -W ~/projects/levelup -/ lu

# Navigate Android whitelabels with luwa keyword (eg `luwa sweetgreen`) with autocomplete
luwa() { cd ~/projects/levelup/whitelabel-android-$1; }
compctl -g '~/projects/levelup/whitelabel-android-*(:s@'$HOME'/projects/levelup/whitelabel-android-@@)' luwa

# Easy access to projects used frequently
alias lul='lu levelup'
alias lula='lu levelup-consumer-android'
alias lulam='lu levelup-android-merchant'
alias lug='lu guides'
alias lud='lu developer.thelevelup.com'
alias swl='cd whitelabel-android-sdk'
alias score='cd levelup-sdk-android'

lulog() { adb "$1" logcat LevelUp:V StrictMode:V ActivityThread:E AndroidRuntime:E *:S;}

export LEVELUP_TEST_ALIAS="gradlew connectedCheck --info --continue"
alias lutestresults="open \$(find . -name index.html)"
alias lutest1="../$LEVELUP_TEST_ALIAS; lutestresults"
alias lutest2="../../$LEVELUP_TEST_ALIAS; lutestresults"
alias lutest3="../../../$LEVELUP_TEST_ALIAS; lutestresults"
alias luuninstall="adb shell 'pm list packages' | grep scvngr | sed 's/package://' | tr -d '\r' | xargs -n1 -t adb uninstall"

# Quick way to rebuild the Launch Services database and get rid
# of duplicates in the Open With submenu.
alias fixopenwith='/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user'
# Quicklook from the terminal
alias ql="qlmanage -p"

if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi
