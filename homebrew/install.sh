#!/bin/sh
#
# Homebrew
#
# This installs some of the common dependencies needed (or at least desired)
# using Homebrew.

# Check for Homebrew
if test ! $(which brew)
then
  echo "  Installing Homebrew for you."
  ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)" > /tmp/homebrew-install.log
fi

# Install homebrew packages
brew install grc coreutils spark zsh ack ctags mvim rbenv ruby-build

# Install homebrew-cask https://github.com/phinze/homebrew-cask
brew tap phinze/cask
brew install brew-cask
brew tap caskroom/versions
brew tap caskroom/fonts
brew update
brew cask install alfred android-file-transfer android-studio sourcetree eclipse-ide shiftit \
  fluid spotify google-drive java6 java7u45 virtualbox sublime-text3 github google-drive \
  cocoa-rest-client audacity kaleidoscope imageoptim handbrake grandperspective nvalt vlc
brew cask alfred link

exit 0
