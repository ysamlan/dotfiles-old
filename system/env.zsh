# Only set this if we haven't set $EDITOR up somewhere else previously.
if [[ "$EDITOR" == "" ]] ; then
  # Use atom for my editor.
  export EDITOR='atom'
fi

# Work around limits in OS X that can mess up test runs.
ulimit -n 10000
