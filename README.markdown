# ysamlan does dotfiles

Personal fork of Zach Holman's [dotfiles](https://github.com/holman/dotfiles),
using more standardized addons for less twiddling.

Main additions:

* Janus for vim
* Sublime Text 3 support
* Oh My ZSH as only out-of-the-box ZSH customizations
* Homebrew + Cask install for a lot of common OS X apps out of the box (see homebrew/install.sh)
